## techship_serial USB serial driver kernel module
This driver is a slightly modified copy of the in-kernel USB serial option driver with a makefile for easy compiling and installation of the kernel module in your existing system build.
Git clone the repository, make sure you have the pre-requirements installed, use makefile to compile and install in system.

### Pre-requirements
The serial driver is based on the in-kernel option driver and need the following kernel configs to be enabled in your existing build.
```text
CONFIG_USB_SERIAL=y
CONFIG_USB_SERIAL_WWAN=y
CONFIG_USB_SERIAL_OPTION=y
CONFIG_USBNET=y
```

You need to have build package and kernel headers installed for your Linux distribution prior to compiling the driver kernel modules. 
On Debian based systems this can typically be installed with apt package manager:
```console
apt-get install build-essential make gcc
apt-get install linux-headers-`uname -r`
```

Navigate to the repository folder and run make to compile the driver module binaries:
```console
make
```
`Make sure you did not get any compile errors, if so typically some library dependencies or build tools are missing or the driver source need to be modified slightly to compile in your Linux distribution and specific kernel version.`

The modified techship_serial driver should not be used in parallel with the in-kernel option usb-serial driver, so please blacklist it:
```console
make blacklist-option
```
Run make install to copy the compiled kernel module binaries to the system builds kernel module folder, run depmod and load the driver:
```console
make install
```
To uninstall the driver use make uninstall to unload driver, remove driver binary from system build folder, run depmod:
```console
make uninstall
```
To restore option driver, use make restore-option argument to remove it from blacklist:
```console
make restore-option
```

`If ModemManager is installed in the system, it typically automatically scan and probe all serial interfaces and can interfere with them if the device is not intended or compatible to be controlled by it.`
Use make stop-modemmanager alternative to temporarily stop the ModemManager service from running until started again or system restarted:
```console
make stop-modemmanager
```
Use make disable-modemmanager alternative to stop and disable the ModemManager service from running in the system (reboot persistent):
```console
make disable-modemmanager
```
Use make enable-modemmanager alternative to enabled and start the ModemManager service:
```console
make enable-modemmanager
```
Use make start-modemmanager alternative to start the ModemManager service:
```console
make start-modemmanager
```
When serial drivers are implemented correctly they will be bound to device interfaces. For example as below:
```console
dmesg
usb 1-1.4: new high-speed USB device number 12 using dwc_otg
usb 1-1.4: New USB device found, idVendor=1bbb, idProduct=01aa, bcdDevice=10.00
usb 1-1.4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
usb 1-1.4: Product: Mobilebroadband
usb 1-1.4: Manufacturer: Alcatel
usb 1-1.4: SerialNumber: 1234567890ABCDE
rndis_host 1-1.4:1.0 usb0: register 'rndis_host' at usb-3f980000.usb-1.4, RNDIS device, 86:35:6b:2a:fb:6d
techship_serial 1-1.4:1.2: Cellular modem (1-port) converter detected
usb 1-1.4: Cellular modem (1-port) converter now attached to ttyUSB0
techship_serial 1-1.4:1.3: Cellular modem (1-port) converter detected
usb 1-1.4: Cellular modem (1-port) converter now attached to ttyUSB1
```
```console
lsusb
Bus 001 Device 012: ID 1bbb:01aa T & A Mobile Phones
```
```console
lsusb -t
|__ Port 4: Dev 12, If 1, Class=CDC Data, Driver=rndis_host, 480M
|__ Port 4: Dev 12, If 2, Class=Vendor Specific Class, Driver=techship_serial, 480M
|__ Port 4: Dev 12, If 0, Class=Wireless, Driver=rndis_host, 480M
|__ Port 4: Dev 12, If 3, Class=Vendor Specific Class, Driver=techship_serial, 480M
```
```console
usb-devices
T:  Bus=01 Lev=02 Prnt=02 Port=03 Cnt=02 Dev#= 12 Spd=480 MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=1bbb ProdID=01aa Rev=10.00
S:  Manufacturer=Alcatel
S:  Product=Mobilebroadband
S:  SerialNumber=1234567890ABCDE
C:  #Ifs= 4 Cfg#= 1 Atr=80 MxPwr=500mA
I:  If#=0x0 Alt= 0 #EPs= 1 Cls=e0(wlcon) Sub=01 Prot=03 Driver=rndis_host
I:  If#=0x1 Alt= 0 #EPs= 2 Cls=0a(data ) Sub=00 Prot=00 Driver=rndis_host
I:  If#=0x2 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=00 Prot=00 Driver=techship_serial
I:  If#=0x3 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=ff Prot=ff Driver=techship_serial
```
The name of the virtual driver devices bound to the cellular modules usb-serial interfaces can be discovered by command:
```console
ls -l /dev/serial/by-id/
total 0
lrwxrwxrwx 1 root root 13 Jun 22 08:13 usb-Alcatel_Mobilebroadband_1234567890ABCDE-if02-port0 -> ../../ttyUSB0
lrwxrwxrwx 1 root root 13 Jun 22 08:13 usb-Alcatel_Mobilebroadband_1234567890ABCDE-if03-port0 -> ../../ttyUSB1
```
