## techship_qmi_wwan USB qmi wwan driver kernel module
This driver is a slightly modified copy of the in-kernel USB qmi_wwan driver with a makefile for easy compiling and installation of the kernel module in your existing system build.
Git clone the repository, make sure you have the pre-requirements installed, use makefile to compile and install in system.

### Pre-requirements
The techship_qmi_wwan driver is almost completely identical to the in-kernel qmi_wwan driver and relies on the following kernel configs to be enabled in your existing build.
```text
CONFIG_USB_USBNET
CONFIG_USB_WDM
```

You need to have build package and kernel headers installed for your Linux distribution prior to compiling the driver kernel modules. 
On Debian based systems this can typically be installed with apt package manager:
```console
apt-get install build-essential make gcc
apt-get install linux-headers-`uname -r`
```

Navigate to the driver folder and run make to compile the driver module binaries:
```console
make
```
`Make sure you did not get any compile errors, if so typically some library dependencies or build tools are missing or the driver source need to be modified slightly to compile in your Linux distribution and specific kernel version.`

The modified techship_qmi_wwan driver should not be used in parallel with the in-kernel qmi_wwan driver, so please blacklist it:
```console
make blacklist-qmi_wwan
```
Run make install to copy the compiled kernel module binaries to the system builds kernel module folder, run depmod and load the driver:
```console
make install
```
To uninstall the driver use make uninstall to unload driver, remove driver binary from system build folder and run depmod use:
```console
make uninstall
```
To restore qmi_wwan driver, use make restore-qmi_wwan argument to remove it from blacklist:
```console
make restore-qmi_wwan
```

`If ModemManager is installed in the system, it typically automatically scan and probe all interfaces and can interfere with them if the device is not intended or compatible to be controlled by it.`
Use make stop-modemmanager alternative to temporarily stop the ModemManager service from running until started again or system restarted:
```console
make stop-modemmanager
```
Use make disable-modemmanager alternative to stop and disable the ModemManager service from running in the system (reboot persistent):
```console
make disable-modemmanager
```
Use make enable-modemmanager alternative to enabled and start the ModemManager service:
```console
make enable-modemmanager
```
Use make start-modemmanager alternative to start the ModemManager service:
```console
make start-modemmanager
```
When drivers are implemented correctly they will be bound to device interfaces. For example as below:
```console
dmesg
usb 2-4: new SuperSpeed Gen 1 USB device number 7 using xhci_hcd
usb 2-4: New USB device found, idVendor=1e0e, idProduct=9001, bcdDevice= 4.14
usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
usb 2-4: Product: SDXPRAIRIE-MTP _SN:
usb 2-4: Manufacturer: QCOM
usb 2-4: SerialNumber: 
techship_serial 2-4:1.0: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB0
techship_serial 2-4:1.1: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB1
techship_serial 2-4:1.2: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB2
techship_serial 2-4:1.3: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB3
techship_serial 2-4:1.4: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB4
qmi_wwan 2-4:1.5: cdc-wdm0: USB WDM device
qmi_wwan 2-4:1.5 wwan0: register 'qmi_wwan' at usb-0000:00:15.0-4, WWAN/QMI device, c2:46:0a:be:98:3f
```
```console
lsusb
Bus 002 Device 006: ID 1e0e:9001 Qualcomm / Option SDXPRAIRIE-MTP _SN:
```
```console
lsusb -t
    |__ Port 4: Dev 6, If 0, Class=Vendor Specific Class, Driver=techship_serial, 5000M
    |__ Port 4: Dev 6, If 1, Class=Vendor Specific Class, Driver=techship_serial, 5000M
    |__ Port 4: Dev 6, If 2, Class=Vendor Specific Class, Driver=techship_serial, 5000M
    |__ Port 4: Dev 6, If 3, Class=Vendor Specific Class, Driver=techship_serial, 5000M
    |__ Port 4: Dev 6, If 4, Class=Vendor Specific Class, Driver=techship_serial, 5000M
    |__ Port 4: Dev 6, If 5, Class=Vendor Specific Class, Driver=qmi_wwan, 5000M
```
```console
usb-devices
T:  Bus=02 Lev=01 Prnt=01 Port=03 Cnt=01 Dev#=  6 Spd=5000 MxCh= 0
D:  Ver= 3.20 Cls=00(>ifc ) Sub=00 Prot=00 MxPS= 9 #Cfgs=  1
P:  Vendor=1e0e ProdID=9001 Rev=04.14
S:  Manufacturer=QCOM
S:  Product=SDXPRAIRIE-MTP _SN:
S:  SerialNumber=
C:  #Ifs= 6 Cfg#= 1 Atr=a0 MxPwr=896mA
I:  If#=0x0 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=ff Prot=30 Driver=techship_serial
I:  If#=0x1 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=60 Driver=techship_serial
I:  If#=0x2 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=40 Driver=techship_serial
I:  If#=0x3 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=40 Driver=techship_serial
I:  If#=0x4 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=40 Driver=techship_serial
I:  If#=0x5 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=50 Driver=qmi_wwan

```