## simcom_wwan USB wwan driver kernel module
This is a alternative USB network driver by Simcom for SIM7500 and SIM7600 series cellular modules when used in QMI/RMNET USB mode (VID 1e0e PID 9001).
Includes a makefile to compile and install the driver in your existing system build. 
See Makefile alternatives for how to blacklist the in-kernel driver when using this driver.

Use AT commands to configure, trigger, manage and control the cellular connection in module.
Use DHCP client on the network interface to in linux host to complete the connection setup after the connection have been set up in module with AT commands.
(This driver is not compatible with ModemManager)

### Pre-requirements
The driver relies on the following kernel parts and the following configs need to be enabled in your current kernel build.
```text
CONFIG_USB_USBNET
CONFIG_USB_WDM
```

You need to have build package and kernel headers installed for your Linux distribution prior to compiling the driver kernel modules. 
On Debian based systems this can typically be installed with apt package manager:
```console
apt-get install build-essential make gcc
apt-get install linux-headers-`uname -r`
```

Navigate to the driver folder and run make to compile the driver module binaries:
```console
make
```
`Make sure you did not get any compile errors, if so typically some library dependencies or build tools are missing or the driver source need to be modified slightly to compile in your Linux distribution and specific kernel version.`

The modified simcom_wwan driver should not be used in parallel with the in-kernel qmi_wwan driver, so please blacklist it:
```console
make blacklist-qmi_wwan
```
Run make install to copy the compiled kernel module binaries to the system builds kernel module folder, run depmod and load the driver:
```console
make install
```
To uninstall the driver use make uninstall to unload driver, remove driver binary from system build folder and run depmod use:
```console
make uninstall
```
To restore qmi_wwan driver, use make restore-qmi_wwan argument to remove it from blacklist:
```console
make restore-qmi_wwan
```

`If ModemManager is installed in the system, it typically automatically scan and probe all interfaces and can interfere with them if the device is not intended or compatible to be controlled by it.`
Use make stop-modemmanager alternative to temporarily stop the ModemManager service from running until started again or system restarted:
```console
make stop-modemmanager
```
Use make disable-modemmanager alternative to stop and disable the ModemManager service from running in the system (reboot persistent):
```console
make disable-modemmanager
```
Use make enable-modemmanager alternative to enabled and start the ModemManager service:
```console
make enable-modemmanager
```
Use make start-modemmanager alternative to start the ModemManager service:
```console
make start-modemmanager
```
When the drivers are implemented correctly they will be bound to device interfaces. For example as below:
```console
dmesg
usb 1-4: new high-speed USB device number 17 using xhci_hcd
usb 1-4: New USB device found, idVendor=1e0e, idProduct=9001, bcdDevice= 3.18
usb 1-4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
usb 1-4: Product: SimTech, Incorporated
usb 1-4: Manufacturer: SimTech, Incorporated
usb 1-4: SerialNumber: 
techship_serial 2-4:1.0: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB0
techship_serial 2-4:1.1: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB1
techship_serial 2-4:1.2: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB2
techship_serial 2-4:1.3: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB3
techship_serial 2-4:1.4: Cellular modem (1-port) converter detected
usb 2-4: Cellular modem (1-port) converter now attached to ttyUSB4
simcom_wwan 1-4:1.5 wwan0: register 'simcom_wwan' at usb-0000:00:15.0-4, SIMCOM wwan/QMI device, c2:46:0a:be:98:3f
```
```console
lsusb
Bus 001 Device 017: ID 1e0e:9001 Qualcomm / Option SimTech, Incorporated
```
```console
lsusb -t
    |__ Port 4: Dev 17, If 0, Class=Vendor Specific Class, Driver=techship_serial, 480M
    |__ Port 4: Dev 17, If 1, Class=Vendor Specific Class, Driver=techship_serial, 480M
    |__ Port 4: Dev 17, If 2, Class=Vendor Specific Class, Driver=techship_serial, 480M
    |__ Port 4: Dev 17, If 3, Class=Vendor Specific Class, Driver=techship_serial, 480M
    |__ Port 4: Dev 17, If 4, Class=Vendor Specific Class, Driver=techship_serial, 480M
    |__ Port 4: Dev 17, If 5, Class=Vendor Specific Class, Driver=simcom_wwan, 480M
```
```console
usb-devices
T:  Bus=01 Lev=01 Prnt=01 Port=03 Cnt=01 Dev#= 17 Spd=480 MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=1e0e ProdID=9001 Rev=03.18
S:  Manufacturer=SimTech, Incorporated
S:  Product=SimTech, Incorporated
S:  SerialNumber=
C:  #Ifs= 6 Cfg#= 1 Atr=a0 MxPwr=500mA
I:  If#=0x0 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=ff Prot=ff Driver=techship_serial
I:  If#=0x1 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=00 Prot=00 Driver=techship_serial
I:  If#=0x2 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=00 Prot=00 Driver=techship_serial
I:  If#=0x3 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=00 Prot=00 Driver=techship_serial
I:  If#=0x4 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=00 Prot=00 Driver=techship_serial
I:  If#=0x5 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=ff Prot=ff Driver=simcom_wwan

```