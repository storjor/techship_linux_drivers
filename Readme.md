# Techship modified Linux driver sources for cellular module interfaces
Modified Linux kernel driver module sources and makefiles for compiling and installing driver modules without needing to rebuild the kernel.

Git clone the repository, install pre-requirements, use makefile to compile and install the kernel driver modules needed for your device.
```console
git clone https://bitbucket.org/storjor/techship_linux_drivers.git
```

### Pre-requirements
You need to have build package and kernel headers installed for your Linux distribution prior to compiling the driver kernel modules.
On Debian based systems this can typically be installed with apt package manager:
```console
apt-get install build-essential make gcc
apt-get install linux-headers-`uname -r`
```

Read more in each driver module folder readme file.

### techship_serial USB serial driver module
A USB serial driver slightly modified version from the in-kernel USB serial option driver.
Includes makefile to compile and install the driver in your system.
See Makefile alternative for how to blacklist the in-kernel driver when using this one.

### techship_qmi_wwan USB wwan network driver module
A USB network driver slightly modified version from the in-kernel USB network qmi_wwan driver.
Includes makefile to compile and install the driver in your existing system build.
See Makefile alternative for how to blacklist the in-kernel driver when using this one.

### simcom_qmi_wwan USB wwan network driver module
A USB network driver slightly modified by Simcom for their 5G SIM820x series cellular modules. Based on the in-kernel USB wwan network driver qmi_wwan.
Includes a makefile to compile and install the driver in your existing system build.
See Makefile alternatives for how to blacklist the in-kernel driver when using this one.

### simcom_wwan USB wwan network driver module
A USB network driver by Simcom for their SIM7500 and SIM7600 series cellular modules using RMNET/QMI mode.
Includes a makefile to compile and install the driver in your existing system build.
See Makefile alternative for how to blacklist the in-kernel driver when using this one.
Use AT commands to configure, trigger, manage and control the cellular connection in module.
Use DHCP client on the network interface, in linux host, after the connection have been set up in module.
